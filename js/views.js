define(["jquery", "underscore", "backbone","moment","bootstrap"],function($, _, Backbone, moment){
  "use strict";
  /* constants */
  var INIT = 'init';
  var FINAL = 'final';
  var RESULTS = 'results';

  var views = {};
  views.HeaderView = Backbone.View.extend({
    template : _.template($("#exton-header").html()),
    initialize : function(options){
      this.phase = this.model.getPhase();
      this.nextPhase = this.model.getNextPhase();
      this.render();
      this.$desc = this.$('.exton-phase-desc');
      this.$vote = this.$('.exton-vote');
      this.setVoteButton();
    },
    render : function(){
      var modelJSON = this.model.toJSON();
      var data = {
        year : modelJSON.year,
        desc : modelJSON.phases[this.phase].desc,
        voteLink : modelJSON.voteLink
      };
      this.$el.html(this.template(data));
      return this;
    },
    setVoteButton : function(){
      if(this.phase !== INIT){
        this.$vote.remove();
      }
    }
  });

  views.HeaderViewOld = Backbone.View.extend({
    template : _.template($("#exton-header-old").html()),
    initialize : function(options){ this.render(); },
    events : { "click .exton-nav li" : "yearClicked"},
    render : function(){
      this.$el.html(this.template);
      return this;
    },
    yearClicked : function(e){
      this.$('.exton-nav li').removeClass('active');
      $(e.currentTarget).addClass('active');
    },
    updateList : function(years){
      var count = 1;
      years.forEach(function(y){
        if(count===1){
          self.$('.exton-nav').append('<li class="active"><a href="#year/'+y+'">'+y+'</a>');
        } else {
          self.$('.exton-nav').append('<li><a href="#year/'+y+'">'+y+'</a>');
        }
        count++;
      });
    }
  });

  views.TeamView = Backbone.View.extend({
    template : _.template($("#modal-team").html()),
    className : "modal hide fade",
    initialize : function(){ },
    render : function(){
      this.$el.attr('id','modal-team-'+this.model.id);
      this.$el.attr('role', 'dialog');
      var data = $.extend(this.model.toJSON(),{images:this.model.getImages()});
      this.$el.html(this.template(data));
      return this;
    }
  });

  views.TeamDescInit = Backbone.View.extend({
    template : _.template($('#small-team-desc').html()),
    tagName : "li",
    className : 'span4',
    events : { 'click' : 'modal' },
    initialize : function(){ },
    render : function(){
      this.$el.html(this.template(this.model.toJSON()));
      return this;
    },
    modal : function(){ $('#modal-team-'+this.model.id).modal(); }
  });

  views.TeamDescFinal = Backbone.View.extend({
    template : _.template($('#medium-team-desc').html()),
    tagName : "li",
    className : 'span6',
    events : { 'click' : 'modal' },
    initialize : function(){ },
    render : function(){
      this.$el.html(this.template(this.model.toJSON()));
      return this;
    },
    modal : function(){ $('#modal-team-'+this.model.id).modal(); }
  });

  views.TeamDescResultsFirst = Backbone.View.extend({
    template : _.template($('#results-team-desc-first').html()),
    tagName : "li",
    className : 'span12',
    events : { 'click' : 'modal' },
    initialize : function(){},
    render : function(){
      this.$el.html(this.template(this.model.toJSON()));
      return this;
    },
    modal : function(){ $('#modal-team-'+this.model.id).modal(); }
  });

  views.TeamDescResults = Backbone.View.extend({
    template : _.template($('#results-team-desc').html()),
    tagName : "li",
    className : 'span4',
    events : { 'click' : 'modal' },
    initialize : function(){},
    render : function(){
      this.$el.html(this.template(this.model.toJSON()));
      return this;
    },
    modal : function(){ $('#modal-team-'+this.model.id).modal(); }
  });
  return views;
});