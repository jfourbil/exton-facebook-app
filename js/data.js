define([],function(){

  var teams = {};
  var prices = {};
  teams.n1 = {
    "id":"1",
    "year":"2013",
    "name":"Agros Contre le Paludisme",
    "school":"Agro ParisTech",
    "website":"http://www.agroparis.com",
    "president":"Albert Truant",
    "logo":"img1.jpg",
    "descAsso":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. In aliquam dictum nunc, eu viverra ante lacinia ut. Vestibulum ornare nibh turpis, sed ullamcorper neque. In hac habitasse platea dictumst. Praesent laoreet diam nisl, id congue ante. Phasellus id placerat odio. Morbi egestas egestas est ut ullamcorper. Suspendisse vehicula faucibus volutpat.",
    "descProject":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vel quam eget massa pretium malesuada sit amet sed erat. Nulla facilisi. Praesent dolor magna, elementum vitae accumsan a, condimentum eu sapien. Integer scelerisque, ligula sit amet lobortis scelerisque, nunc nibh tempus quam, id venenatis dui justo ac arcu. Fusce elementum arcu ac turpis vehicula a tempus nisl pellentesque. In vitae vehicula enim. Vestibulum placerat massa nunc.",
    "synthesis" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eget mi vel mauris placerat sollicitudin ut nec mauris. Vestibulum et velit justo, ac viverra magna. Aenean elementum dui sed diam rutrum facilisis. Suspendisse velit ante, fringilla at posuere ac, lacinia vitae augue. In hac amet.",
    "imgs":"a nice caption.--img1n1.jpg__a sheep--img1n2.jpg"
  };
  teams.n2 = {
    "id":"2",
    "year":"2013",
    "name":"Mission Potosi",
    "school":"ESSEC, Polytechnique et Médecine Paris V",
    "website":"http://www.agroparis.com",
    "president":"Albert Truant",
    "logo":"img2.jpg",
    "descAsso":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. In aliquam dictum nunc, eu viverra ante lacinia ut. Vestibulum ornare nibh turpis, sed ullamcorper neque. In hac habitasse platea dictumst. Praesent laoreet diam nisl, id congue ante. Phasellus id placerat odio. Morbi egestas egestas est ut ullamcorper. Suspendisse vehicula faucibus volutpat. @&éèç$ùà",
    "descProject":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vel quam eget massa pretium malesuada sit amet sed erat. Nulla facilisi. Praesent dolor magna, elementum vitae accumsan a, condimentum eu sapien. Integer scelerisque, ligula sit amet lobortis scelerisque, nunc nibh tempus quam, id venenatis dui justo ac arcu. Fusce elementum arcu ac turpis vehicula a tempus nisl pellentesque. In vitae vehicula enim. Vestibulum placerat massa nunc.",
    "synthesis" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eget mi vel mauris placerat sollicitudin ut nec mauris. Vestibulum et velit justo, ac viverra magna. Aenean elementum dui sed diam rutrum facilisis. Suspendisse velit ante, fringilla at posuere ac, lacinia vitae augue. In hac amet.",
    "imgs":"a nice caption.--img1n1.jpg__a sheep--img1n2.jpg"
  };
  teams.n3 = {
    "id":"3",
    "year":"2013",
    "name":"Association Schola Africa",
    "school":"EDHEC Business school",
    "website":"http://www.agroparis.com",
    "president":"Albert Truant",
    "logo":"img3.jpg",
    "descAsso":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. In aliquam dictum nunc, eu viverra ante lacinia ut. Vestibulum ornare nibh turpis, sed ullamcorper neque. In hac habitasse platea dictumst. Praesent laoreet diam nisl, id congue ante. Phasellus id placerat odio. Morbi egestas egestas est ut ullamcorper. Suspendisse vehicula faucibus volutpat.",
    "descProject":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vel quam eget massa pretium malesuada sit amet sed erat. Nulla facilisi. Praesent dolor magna, elementum vitae accumsan a, condimentum eu sapien. Integer scelerisque, ligula sit amet lobortis scelerisque, nunc nibh tempus quam, id venenatis dui justo ac arcu. Fusce elementum arcu ac turpis vehicula a tempus nisl pellentesque. In vitae vehicula enim. Vestibulum placerat massa nunc.",
    "synthesis" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eget mi vel mauris placerat sollicitudin ut nec mauris. Vestibulum et velit justo, ac viverra magna. Aenean elementum dui sed diam rutrum facilisis. Suspendisse velit ante, fringilla at posuere ac, lacinia vitae augue. In hac amet.",
    "imgs":"a nice caption.--img1n1.jpg__a sheep--img1n2.jpg"
  };
  teams.n4 = {
    "id":"4",
    "year":"2013",
    "name":"Are you ready ToGO",
    "school":"Centrale Lyon et EM LYON Business School",
    "website":"http://www.agroparis.com",
    "president":"Albert Truant",
    "logo":"img1.jpg",
    "descAsso":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. In aliquam dictum nunc, eu viverra ante lacinia ut. Vestibulum ornare nibh turpis, sed ullamcorper neque. In hac habitasse platea dictumst. Praesent laoreet diam nisl, id congue ante. Phasellus id placerat odio. Morbi egestas egestas est ut ullamcorper. Suspendisse vehicula faucibus volutpat.",
    "descProject":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vel quam eget massa pretium malesuada sit amet sed erat. Nulla facilisi. Praesent dolor magna, elementum vitae accumsan a, condimentum eu sapien. Integer scelerisque, ligula sit amet lobortis scelerisque, nunc nibh tempus quam, id venenatis dui justo ac arcu. Fusce elementum arcu ac turpis vehicula a tempus nisl pellentesque. In vitae vehicula enim. Vestibulum placerat massa nunc.",
    "synthesis" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eget mi vel mauris placerat sollicitudin ut nec mauris. Vestibulum et velit justo, ac viverra magna. Aenean elementum dui sed diam rutrum facilisis. Suspendisse velit ante, fringilla at posuere ac, lacinia vitae augue. In hac amet.",
    "imgs":"a nice caption.--img1n1.jpg__a sheep--img1n2.jpg"
  };
  teams.n5 = {
    "id":"5",
    "year":"2013",
    "name":"Association NPT",
    "school":"EDHEC Business school",
    "website":"http://www.agroparis.com",
    "president":"Albert Truant",
    "logo":"img2.jpg",
    "descAsso":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. In aliquam dictum nunc, eu viverra ante lacinia ut. Vestibulum ornare nibh turpis, sed ullamcorper neque. In hac habitasse platea dictumst. Praesent laoreet diam nisl, id congue ante. Phasellus id placerat odio. Morbi egestas egestas est ut ullamcorper. Suspendisse vehicula faucibus volutpat.",
    "descProject":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vel quam eget massa pretium malesuada sit amet sed erat. Nulla facilisi. Praesent dolor magna, elementum vitae accumsan a, condimentum eu sapien. Integer scelerisque, ligula sit amet lobortis scelerisque, nunc nibh tempus quam, id venenatis dui justo ac arcu. Fusce elementum arcu ac turpis vehicula a tempus nisl pellentesque. In vitae vehicula enim. Vestibulum placerat massa nunc.",
    "synthesis" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eget mi vel mauris placerat sollicitudin ut nec mauris. Vestibulum et velit justo, ac viverra magna. Aenean elementum dui sed diam rutrum facilisis. Suspendisse velit ante, fringilla at posuere ac, lacinia vitae augue. In hac amet.",
    "imgs":"a nice caption.--img1n1.jpg__a sheep--img1n2.jpg"
  };
  teams.n6 = {
    "id":"6",
    "year":"2013",
    "name":"TPT",
    "school":"EDHEC Business school",
    "website":"http://www.agroparis.com",
    "president":"Albert Truant",
    "logo":"img3.jpg",
    "descAsso":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. In aliquam dictum nunc, eu viverra ante lacinia ut. Vestibulum ornare nibh turpis, sed ullamcorper neque. In hac habitasse platea dictumst. Praesent laoreet diam nisl, id congue ante. Phasellus id placerat odio. Morbi egestas egestas est ut ullamcorper. Suspendisse vehicula faucibus volutpat.",
    "descProject":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vel quam eget massa pretium malesuada sit amet sed erat. Nulla facilisi. Praesent dolor magna, elementum vitae accumsan a, condimentum eu sapien. Integer scelerisque, ligula sit amet lobortis scelerisque, nunc nibh tempus quam, id venenatis dui justo ac arcu. Fusce elementum arcu ac turpis vehicula a tempus nisl pellentesque. In vitae vehicula enim. Vestibulum placerat massa nunc.",
    "synthesis" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eget mi vel mauris placerat sollicitudin ut nec mauris. Vestibulum et velit justo, ac viverra magna. Aenean elementum dui sed diam rutrum facilisis. Suspendisse velit ante, fringilla at posuere ac, lacinia vitae augue. In hac amet.",
    "imgs":"a nice caption.--img1n1.jpg__a sheep--img1n2.jpg"
  };
  prices.n1 = {
    "id":"1",
    "year":"2013",
    "activated":"1",
    "finalists":"1-2-3-4",
    "phases":{
      "init":{
        "desc":"a nice init description",
        "date":"2013-03-14"
      },
      "final":{
        "desc":"a nice final description",
        "date":"2013-03-14"
      },
      "results":{
        "desc":"a nice results description",
        "date":"2013-03-14"
      }
    }
  };
  prices.n2 = {
    "id":"1",
    "year":"2013",
    "activated":"1",
    "finalists":"1-2-3-4",
    "phases":{
      "init":{
        "desc":"a nice init description",
        "date":"2013-03-13"
      },
      "final":{
        "desc":"a nice final description",
        "date":"2013-03-14"
      },
      "results":{
        "desc":"a nice results description",
        "date":"2013-03-15"
      }
    }
  };
  prices.n3 = {
    "id":"1",
    "year":"2013",
    "activated":"1",
    "finalists":"1-2-3-4",
    "phases":{
      "init":{
        "desc":"a nice init description",
        "date":"2013-03-12"
      },
      "final":{
        "desc":"a nice final description",
        "date":"2013-03-13"
      },
      "results":{
        "desc":"a nice results description",
        "date":"2013-03-14"
      }
    }
  };
  return {
    prices : prices,
    teams : teams
  };
});