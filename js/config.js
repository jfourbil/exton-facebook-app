/*jshint*/
/*global require*/
(function (){
  "use strict";
  require.config({
    paths: {
      // ext lib aliases
      'jquery': '../lib/jquery-1.9.1.min',
      'underscore': '../lib/underscore-min',
      'backbone': '../lib/backbone-min',
      'moment': '../lib/moment.min',
      'bootstrap': '../lib/bootstrap.min',
      'datepicker': '../lib/bootstrap-datepicker',
      'iframe-transport' : '../lib/jquery.iframe-transport',

      'admin' : '../api/templates'
    },
    shim: {
      // dealing with dependencies and exports of non-AMD modules
      'underscore': {
        exports: '_'
      },
      'backbone': {
        deps: ['jquery','underscore'],
        exports: 'Backbone'
      },
      'bootstrap': {
        deps: ['jquery']
      },
      'datepicker' : {
        deps: ['jquery','bootstrap']
      },
      'iframe-transport' : {
        deps : ['jquery']
      }
    }
  });

  require(['moment'],function (moment){
    /* set french language to momentjs */
    moment.lang('fr', {
      months : "janvier_février_mars_avril_mai_juin_juillet_août_septembre_octobre_novembre_décembre".split("_"),
      monthsShort : "janv._févr._mars_avr._mai_juin_juil._août_sept._oct._nov._déc.".split("_"),
      weekdays : "dimanche_lundi_mardi_mercredi_jeudi_vendredi_samedi".split("_"),
      weekdaysShort : "dim._lun._mar._mer._jeu._ven._sam.".split("_"),
      weekdaysMin : "Di_Lu_Ma_Me_Je_Ve_Sa".split("_"),
      longDateFormat : {
        LT : "HH:mm",
        L : "DD/MM/YYYY",
        LL : "D MMMM YYYY",
        LLL : "D MMMM YYYY LT",
        LLLL : "dddd D MMMM YYYY LT"
      },
      calendar : {
        sameDay: "[Aujourd'hui à] LT",
        nextDay: '[Demain à] LT',
        nextWeek: 'dddd [à] LT',
        lastDay: '[Hier à] LT',
        lastWeek: 'dddd [dernier à] LT',
        sameElse: 'L'
      },
      relativeTime : {
        future : "dans %s",
        past : "il y a %s",
        s : "quelques secondes",
        m : "une minute",
        mm : "%d minutes",
        h : "une heure",
        hh : "%d heures",
        d : "un jour",
        dd : "%d jours",
        M : "un mois",
        MM : "%d mois",
        y : "une année",
        yy : "%d années"
      },
      ordinal : function (number) {
        return number + (number === 1 ? 'er' : 'ème');
      },
      week : {
        dow : 1, // Monday is the first day of the week.
        doy : 4  // The week that contains Jan 4th is the first week of the year.
      }
    });
  });
}());