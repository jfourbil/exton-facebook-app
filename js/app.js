/*jshint*/
/*global require*/

require(['./config'], function () {
  "use strict";
  console.log('###app###');
  // detect the IE's version
  var ie = function(){
    var undef,
    v = 3,
    div = document.createElement('div'),
    all = div.getElementsByTagName('i');
    while (
      div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
      all[0]
      );
      return v > 4 ? v : undef;
  };
  // if ie is defined and if ie's version is older than 9
  // it's displayed and error
  if(ie() && ie()<=9){
    require(['jquery','underscore'], function($,_){
      var error = _.template($("#error-template").html());
      $('.container').html(error);
    });
  } else {
    require(['router'], function(Router){
      var router = new Router();
      Backbone.history.start();
    });
  }
});