define(["jquery","underscore","backbone","models","views","data","./fb!"],function($,_,Backbone,Models,Views,data){
    "use strict";
    /* constants */
    var INIT = 'init';
    var FINAL = 'final';
    var RESULTS = 'results';

    var Router = Backbone.Router.extend({
      routes : {
        "":"home",
        "phase/:id":"applyPhase",
        "old/" : "list",
        "year/:year" : "displayYear"
      },
      initialize : function(){
        this.$teams = $('#teams');
        this.$hero = $('#hero');
      },
      displayYear : function (year){
        this.$teams.empty();
        this.$teams.html(_.template($('#'+RESULTS+'-layout').html()));
        this.currentPrice = this.prices.filter(function(p){ return p.get('year')===year;})[0];
        this.teams = this.teamsByYears[year];
        this.displayTeams(RESULTS);
      },
      list : function(){
        var self = this;
        this.prices = new Models.Prices();
        this.teamsByYears = {};
        this.$teams.empty();
        this.$hero.empty();
        var header = new Views.HeaderViewOld({el:"#hero"});
        this.prices.fetch({
          success : function(data){
            // get all previous year
            var years = data.map(function(e){ return e.get("year"); });
            // display this year list on the header
            header.updateList(years);
            // get the last year edition
            var previousYear = years[0];
            // apply the results layout
            self.$teams.html(_.template($('#'+RESULTS+'-layout').html()));
            // fetch teams for every editions
            years.forEach(function(y){
              self.teamsByYears[y] = new Models.Teams({year : y});
              self.teamsByYears[y].fetch({
                // if this year edition is the last one then when display the results otherwise we do nothing
                success : function(){ if(y===previousYear){ self.displayYear(previousYear); } }
              });
            });
          }
        });
      },
      applyPhaseLayout : function(){
        var currPhase = this.currentPrice.getPhase() || INIT;
        this.$teams.empty();
        this.$teams.html(_.template($('#'+currPhase+'-layout').html()));
      },
      home : function(){
        this.$teams.empty();
        this.$hero.empty();
        var self = this;
        // load current price
        this.currentPrice = new Models.CurrentPrice();
        this.currentPrice.fetch({
          success : function(data){
            new Views.HeaderView({el : '#hero', model : data});
            self.applyPhaseLayout();
            // load corresponding teams
            self.teams = new Models.Teams({year : data.get('year')});
            self.teams.fetch({
              success : function(data){
                self.displayTeams(self.currentPrice.getPhase());
              }
            });
          }
        });
      },
      _ensureModal : function(model){
        var modal = $('#modal-team-'+model.id);
        modal.remove();
      },
      displayTeams : function(phase){
        var self = this;
        if(phase===INIT){
          this.teams.shuffle().forEach(function(t){
            $('.exton-small-list').append(new Views.TeamDescInit({model : t}).render().el);
            self._ensureModal(t);
            $('body').append(new Views.TeamView({model : t}).render().el);
          });
        } else {
          var f = this.currentPrice.getFinalists();
          var finalists = f.map(function(idTeam){
            var fArray = self.teams.filter(function(t){
              return t.id === idTeam;
            });
            return fArray[0];
          });
          if (phase===FINAL){
            _.shuffle(finalists).forEach(function(t){
              $('.exton-medium-list').append(new Views.TeamDescFinal({model : t}).render().el);
              self._ensureModal(t);
              $('body').append(new Views.TeamView({model : t}).render().el);
            });
          } else if(phase===RESULTS){
            // TODO
            var i=0;
            finalists.forEach(function(t){
              if(i===0){
                $('.exton-results-list').append(new Views.TeamDescResultsFirst({model : t}).render().el);
                self._ensureModal(t);
                $('body').append(new Views.TeamView({model : t}).render().el);
              } else {
                $('.exton-results-list').append(new Views.TeamDescResults({model : t}).render().el);
                $('body').append(new Views.TeamView({model : t}).render().el);
              }
              i++;
            });
          }
        }
      },
      applyPhase : function(id){
        this.currentPrice = new Models.CurrentPrice(data.prices['n'+id]);
        new Views.HeaderView({el : '#hero', model : this.currentPrice});
        this.applyPhaseLayout();
        var models = [];
        for(var key in data.teams){
          models.push(new Models.Team(data.teams[key]));
        }
        this.teams = new Models.Teams(models);
        this.displayTeams(this.currentPrice.getPhase());
      }
    });
    return Router;
  });