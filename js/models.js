define(["jquery", "underscore", "backbone","moment"],function($, _, Backbone, moment){
  "use strict";
  /* constants */
  var INIT = 'init';
  var FINAL = 'final';
  var RESULTS = 'results';

  var models = {};
  models.CurrentPrice = Backbone.Model.extend({
    urlRoot : "./api/currentPrice",
    getPhase : function(){
      var endFinal = moment(this.get('phases').final.date);
      var endResults = moment(this.get('phases').results.date);
      var start = moment(new Date());

      if(start.isBefore(endFinal)){ return INIT; }
      if(start.isBefore(endResults)){ return FINAL; }
      return RESULTS;
    },
    getFinalists : function(){
      // pattern : 'idTeam1-idTeam2'
      var f = this.get('finalists');
      return f.split("-");
    },
    getNextPhase : function(){
      var endInit = moment(this.get('phases').init.date);
      var endFinal = moment(this.get('phases').final.date);
      var start = moment(new Date());
      if(start.isBefore(endInit)){ return INIT; }
      if(start.isBefore(endFinal)){ return FINAL; }
      return RESULTS;
    }
  });

  models.Prices = Backbone.Collection.extend({
    url : "./api/prices/previous",
    model : models.CurrentPrice
  });

  models.Team = Backbone.Model.extend({
    urlRoot : './api/team',
    idAttribute : 'id',
    getImages : function(){
      // split images because only names are stored in the DB
      // pattern : image1.jpg__image2.jpg 
      var imgs = this.get('imgs');
      var tab = [];
      if(imgs !== ''){ tab = imgs.split('__'); }
      return tab;
    }
  });

  models.Teams = Backbone.Collection.extend({
    model : models.Team,
    initialize : function(options){
      this.url = './api/teams/'+ options.year;
    }
  });

  return models;
});