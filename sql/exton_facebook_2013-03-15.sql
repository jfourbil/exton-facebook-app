# ************************************************************
# Sequel Pro SQL dump
# Version 4004
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Hôte: localhost (MySQL 5.5.25)
# Base de données: exton_facebook
# Temps de génération: 2013-03-15 09:32:01 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Affichage de la table prices
# ------------------------------------------------------------

DROP TABLE IF EXISTS `prices`;

CREATE TABLE `prices` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `year` int(4) DEFAULT NULL,
  `init_date` date DEFAULT NULL,
  `init_desc` text COLLATE utf8_unicode_ci,
  `final_date` date DEFAULT NULL,
  `final_desc` text COLLATE utf8_unicode_ci,
  `results_date` date DEFAULT NULL,
  `results_desc` text COLLATE utf8_unicode_ci,
  `activated` tinyint(1) DEFAULT NULL,
  `finalists` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `voteLink` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `year_2` (`year`),
  KEY `year` (`year`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `prices` WRITE;
/*!40000 ALTER TABLE `prices` DISABLE KEYS */;

INSERT INTO `prices` (`id`, `year`, `init_date`, `init_desc`, `final_date`, `final_desc`, `results_date`, `results_desc`, `activated`, `finalists`, `voteLink`)
VALUES
	(1,2013,'2013-03-12','a nice init déscription @çç!è%','2013-03-18','a nice final description','2013-03-28','a nice results description',1,'1-2-3-4','http://fr.surveymonkey.com/'),
	(2,2012,'2012-03-04','a nice init description','2012-03-06','a nice final description','2012-03-06','a nice results description',1,'1-2-3-4','http://fr.surveymonkey.com/');

/*!40000 ALTER TABLE `prices` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table teams
# ------------------------------------------------------------

DROP TABLE IF EXISTS `teams`;

CREATE TABLE `teams` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `year` int(4) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `president` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `school` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imgSemi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imgFinal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imgResults` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imgFirst` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descAsso` text COLLATE utf8_unicode_ci,
  `descProject` text COLLATE utf8_unicode_ci,
  `imgs` text COLLATE utf8_unicode_ci,
  `synthesis` text COLLATE utf8_unicode_ci,
  `presWords` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;

INSERT INTO `teams` (`id`, `year`, `name`, `president`, `school`, `website`, `logo`, `imgSemi`, `imgFinal`, `imgResults`, `imgFirst`, `descAsso`, `descProject`, `imgs`, `synthesis`, `presWords`)
VALUES
	(1,2013,'Agros Contre le Paludisme','Albert Truant','Agro ParisTech','http://www.agroparis.com','team1logo.png','team1imgSemi.jpg','team1imgFinal.gif','team1imgResults.jpg',NULL,'« Apporter une aide concrète et durable aux personnes et communautés en difficulté »\nSolidari’Terre, issue de la fusion entre les deux associations humanitaires de l’EMLyon et de Centrale Lyon, organise de nombreux projets de solidarité : soutien scolaire, Téléthon, campagnes de dons de sang, mais aussi de nombreuses missions de plus grande envergure.','« De l’eau pour Sumba ». Chaque année depuis 2011, nous aidons l’ONG locale Yayasan Harapan Sumba (« Espoir pour Sumba ») à construire entre 7 et 10 réservoirs d’eau, mais aussi des puits et toilettes sèches dans des villages de cette île pauvre et délaissée d’Indonésie. Pour cela, nous récoltons tout au long de l’année de l’argent pour financer une fois sur place l’achat des matériaux pour la construction des réservoirs. L’immersion du groupe de 10 à 15 étudiants pendant le mois de Juillet est totale, ils vivent aux côtés des locaux et partagent leurs préoccupations quotidiennes.','team1img1.jpg__team1img2.jpg__team1img3.jpg','Synthèse : Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eget mi vel mauris placerat sollicitudin ut nec mauris. Vestibulum et velit justo, ac viverra magna. Aenean elementum dui sed diam rutrum facilisis. Suspendisse velit ante, fringilla at posuere ac, lacinia vitae augue. In hac amet.','Apporter une aide concrète et durable aux personnes et communautés en difficulté'),
	(2,2013,'Mission Potosi','Albert Truant','ESSEC, Polytechnique et Médecine Paris V','http://www.agroparis.com','team2logo.png','team2imgSemi.jpg','team2imgFinal.gif','team2imgResults.jpg',NULL,'« Apporter une aide concrète et durable aux personnes et communautés en difficulté »\nSolidari’Terre, issue de la fusion entre les deux associations humanitaires de l’EMLyon et de Centrale Lyon, organise de nombreux projets de solidarité : soutien scolaire, Téléthon, campagnes de dons de sang, mais aussi de nombreuses missions de plus grande envergure.','« De l’eau pour Sumba ». Chaque année depuis 2011, nous aidons l’ONG locale Yayasan Harapan Sumba (« Espoir pour Sumba ») à construire entre 7 et 10 réservoirs d’eau, mais aussi des puits et toilettes sèches dans des villages de cette île pauvre et délaissée d’Indonésie. Pour cela, nous récoltons tout au long de l’année de l’argent pour financer une fois sur place l’achat des matériaux pour la construction des réservoirs. L’immersion du groupe de 10 à 15 étudiants pendant le mois de Juillet est totale, ils vivent aux côtés des locaux et partagent leurs préoccupations quotidiennes.','team2img1.jpg__team2img2.jpg','',''),
	(3,2013,'Association Schola Africa','Albert Truant','EDHEC Business school','http://www.agroparis.com','team3logo.png','team3imgSemi.jpg','team3imgFinal.gif','team3imgResults.jpg',NULL,'« Apporter une aide concrète et durable aux personnes et communautés en difficulté »\nSolidari’Terre, issue de la fusion entre les deux associations humanitaires de l’EMLyon et de Centrale Lyon, organise de nombreux projets de solidarité : soutien scolaire, Téléthon, campagnes de dons de sang, mais aussi de nombreuses missions de plus grande envergure.','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed condimentum, purus et imperdiet ullamcorper, sem nibh scelerisque mi, quis pretium sapien tortor eu mi. Vivamus erat nibh, ultrices at imperdiet in, consectetur vel nisl. Nam a quam sed lorem scelerisque adipiscing.','team3img1.jpg','',''),
	(4,2013,'Are you ready ToGO','Albert Truant','Centrale Lyon et EM LYON Business School','http://www.agroparis.com','team4logo.png','team4imgSemi.jpg','team4imgFinal.gif','team4imgResults.jpg',NULL,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed condimentum, purus et imperdiet ullamcorper, sem nibh scelerisque mi, quis pretium sapien tortor eu mi.','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed condimentum, purus et imperdiet ullamcorper, sem nibh scelerisque mi, quis pretium sapien tortor eu mi. Vivamus erat nibh, ultrices at imperdiet in, consectetur vel nisl. Nam a quam sed lorem scelerisque adipiscing.','team4img1.jpg__team4img2.jpg','',''),
	(5,2013,'Association NPT','Albert Truant','EDHEC Business school','http://www.agroparis.com','team5logo.png','team5imgSemi.jpg',NULL,NULL,NULL,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed condimentum, purus et imperdiet ullamcorper, sem nibh scelerisque mi, quis pretium sapien tortor eu mi.','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed condimentum, purus et imperdiet ullamcorper, sem nibh scelerisque mi, quis pretium sapien tortor eu mi. Vivamus erat nibh, ultrices at imperdiet in, consectetur vel nisl. Nam a quam sed lorem scelerisque adipiscing.','team5img1.jpg','',NULL),
	(6,2013,'TPT','Albert Truant','EDHEC Business school','http://www.agroparis.com','team6logo.png','team6imgSemi.jpg',NULL,NULL,NULL,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed condimentum, purus et imperdiet ullamcorper, sem nibh scelerisque mi, quis pretium sapien tortor eu mi.','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed condimentum, purus et imperdiet ullamcorper, sem nibh scelerisque mi, quis pretium sapien tortor eu mi. Vivamus erat nibh, ultrices at imperdiet in, consectetur vel nisl. Nam a quam sed lorem scelerisque adipiscing.','team6img1.jpg','',NULL),
	(43,2013,'myTeam1é','Marc Grue','X','http://www.google.com','team43logo.png','team43imgSemi.jpg',NULL,NULL,NULL,'une belle description d\'équipe qui ne fait pas 300 caractères.','une belle description d\'équipe qui ne fait pas 400 caractères.','team43img1.jpg','une belle description d\'équipe qui ne fait pas 300 caractères.',NULL),
	(44,2013,'myteam2','','Centrale','','team44logo.png','team44imgSemi.jpg',NULL,NULL,NULL,'','','team44img1.jpg__team44img2.jpg','',NULL),
	(45,2013,'myteam3','Gîles François','ESCP','http://www.escp.eu','team45logo.png','team45imgSemi.jpg','team45imgFinal.gif','team45imgResults.jpg','team45imgFirst.jpg','« Apporter une aide concrète et durable aux personnes et communautés en difficulté » Solidari’Terre, issue de la fusion entre les deux associations humanitaires de l’EMLyon et de Centrale Lyon, organise de nombreux projets de solidarité : soutien scolaire, Téléthon, campagnes de dons de sang, mais aussi de nombreuses missions de plus grande envergure.','« De l’eau pour Sumba ». Chaque année depuis 2011, nous aidons l’ONG locale Yayasan Harapan Sumba (« Espoir pour Sumba ») à construire entre 7 et 10 réservoirs d’eau, mais aussi des puits et toilettes sèches dans des villages de cette île pauvre et délaissée d’Indonésie. Pour cela, nous récoltons tout au long de l’année de l’argent pour financer une fois sur place l’achat des matériaux pour la construction des réservoirs. L’immersion du groupe de 10 à 15 étudiants pendant le mois de Juillet est totale, ils vivent aux côtés des locaux et partagent leurs préoccupations quotidiennes.','team45img1.jpg','',NULL);

/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
