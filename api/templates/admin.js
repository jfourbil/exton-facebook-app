/*jshint*/
/*global require*/

require(['./config'], function () {
  "use strict";
  console.log('###app###');
  require(['admin/router'], function(Router){
    var router = new Router();
    Backbone.history.start();
  });
});