define(["jquery","underscore","backbone","admin/models","admin/views","bootstrap","iframe-transport"],function($,_,Backbone,Models,Views){
  "use strict";
  var Router = Backbone.Router.extend({
    routes : {
      "":"home",
      "prices" : "listPrices",
      "add/price" : "addPrices",
      "add/price/:id" : "addPrices",
      "delete/price/:id" : "deletePrice",
      "teams" : "listTeams",
      "add/team" : "addTeams",
      "add/team/:id" : "addTeams",
      "delete/team/:id" : "deleteTeam"
    },
    initialize : function(){
      var self = this;
      console.log('#admin loaded#');
      this.$content = $('.content');
      this.teams = new Models.AllTeams();
      this.teams.fetch({
        success : function(data){
          console.log('teams fetched');
        }
      });
      this.prices = new Models.Prices();
      this.prices.fetch({
        success : function(data){
          console.log('prices fetched');
        }
      });
      this.prices.on('remove', function(d){
        $.get('price/delete/'+d.id).complete(function(data){
          self.prices.fetch();
        });
      });
      this.teams.on('remove', function(d){
        $.get('team/delete/'+d.id).complete(function(data){
          self.teams.fetch();
        });
      });
    },
    home : function(){
      console.log('###home###');
      this._cleanLayout();
    },
    _cleanLayout : function(){
      if(this.currentLayout){
        this.currentLayout.remove();
      }
    },
    _applyLayout : function(){
      this.$content.html(this.currentLayout.render().el);
    },
    deletePrice : function(id){
      if(id){
        if(confirm('prix supprimé.')){
          var model = this.prices.get(id);
          this.prices.remove(model);
        }
        window.location.hash = "prices";
      } else {
        alert('prix inexistant');
      }
    },
    addPrices : function(id){
      var self = this;
      this._cleanLayout();
      var model = id ? this.prices.get(id) : new Models.Price();
      var view = this.currentLayout = new Views.priceEdit({model : model});
      this._applyLayout();
      $('#addPrice').submit(function(){
        $.post(id?'price/'+id:'price', model.toJSON())
          .complete(function(data){
            self.prices.fetch({
              success : function(data){
                alert('prix enregistré.');
              }
            });
          });
        return false;
      });
      model.on('delete', function(){
        this.prices.remove(model);
      },this);
    },
    listPrices : function(){
      this._cleanLayout();
      var view = this.currentLayout = new Views.pricesLayout();
      this._applyLayout();
      this.prices.forEach(function(p){
        view.$('tbody').append(new Views.priceView({model : p}).render().el);
      });
    },
    deleteTeam : function(id){
      if(id){
        if(confirm('équipe supprimée.')){
          var model = this.teams.get(id);
          this.teams.remove(model);
        }
        window.location.hash = "teams";
      } else {
        alert('équipe inexistante');
      }
    },
    addTeams : function(id){
      var self = this;
      this._cleanLayout();
      var model = id ? this.teams.get(id) : new Models.Team();
      var view = this.currentLayout = new Views.teamEdit({model : model});
      this._applyLayout();
      $('#addTeam').submit(function(){
        $.ajax(id?'team/'+id:'team', {
          data : JSON.stringify(model.getInputTexts()),
          files : $(':file', this),
          iframe : true,
          processData: false
        }).complete(function(data){
          self.teams.fetch({
            success : function(data){
              alert('équipe enregistrée.');
            }
          });
        });
        return false;
      });
      model.on('delete', function(){
        this.teams.remove(model);
      },this);
    },
    listTeams : function(){
      this._cleanLayout();
      var view = this.currentLayout = new Views.teamsLayout();
      this._applyLayout();
      this.teams.forEach(function(t){
        view.$('tbody').append(new Views.teamView({model : t}).render().el);
      });
    }
  });
  return Router;
});