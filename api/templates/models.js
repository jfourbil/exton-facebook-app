define(["jquery", "underscore", "backbone","moment"],function($, _, Backbone, moment){
  "use strict";

  var models = {};

  models.Price = Backbone.Model.extend({
    urlRoot : "./price",
    getPhase : function(){
      var endFinal = moment(this.get('phases').final.date);
      var endResults = moment(this.get('phases').results.date);
      var start = moment(new Date());

      if(start.isBefore(endFinal)){ return INIT; }
      if(start.isBefore(endResults)){ return FINAL; }
      return RESULTS;
    },
    getFinalists : function(){
      var f = this.get('finalists');
      return f.split("-");
    }
  });

  models.Team = Backbone.Model.extend({
    urlRoot : './team',
    idAttribute : 'id',
    getImages : function(){
      // split images
      // pattern : image1.jpg__img2.jpg 
      var tab = [];

      var imgs = this.get('imgs');
      if(imgs === undefined){ console.log(true);}
      if(imgs !== '' && imgs !== undefined){ tab = imgs.split('__'); }
      return tab;
    },
    getInputTexts : function(){
      return {
        descAsso: this.get("descAsso"),
        descProject: this.get("descProject"),
        name: this.get("name"),
        numImgs: this.get("numImgs"),
        president: this.get("president"),
        school: this.get("school"),
        synthesis: this.get("synthesis"),
        website: this.get("website"),
        year: this.get("year"),
        presWords : this.get("presWords")
      };
    }
  });

  models.AllTeams = Backbone.Collection.extend({
    url : './teams',
    model : models.Team
  });

  models.Prices = Backbone.Collection.extend({
    url : './prices',
    model : models.Price
  });
  return models;
});