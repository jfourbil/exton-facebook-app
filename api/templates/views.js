define(["jquery", "underscore", "backbone","moment","bootstrap", "datepicker"],function($, _, Backbone, moment){
  "use strict";
  var views = {};
  var MAX_IMGS = 6;
  var TEAM_LOGO = "logo";
  var TEAM_IMG_SEMI = "imgSemi";
  var TEAM_IMG_FINAL = "imgFinal";
  var TEAM_IMG_RESULTS = "imgResults";
  var TEAM_IMG_FIRST = "imgFirst";
  var TEAM_IMG_CAROUSEL = "carousel";
  var PRICE_ACTIVATED = "activated";
  var PRICE_YEAR = "year";
  var PRICE_FINALISTS = 'finalists';
  var PRICE_PHASES = 'phases';
  var PRICE_VOTE = 'voteLink';
  var INIT = 'init';
  var FINAL = 'final';
  var RESULTS = 'results';
  var DATE = 'date';
  var DESC = 'desc';

  views.teamsLayout = Backbone.View.extend({
    template : _.template($('#teams-layout').html()),
    tagName : "table",
    className : "table table-striped teams",
    initialize : function(){},
    render : function(){
      this.$el.html(this.template());
      return this;
    }
  });

  views.teamView = Backbone.View.extend({
    template : _.template($('#team-view').html()),
    tagName : 'tr',
    initialize : function(){},
    render : function(){
      this.$el.html(this.template(this.model.toJSON()));
      return this;
    }
  });

  views.teamViewImg = Backbone.View.extend({
    template : _.template($('#team-view-img').html()),
    className : 'control-group',
    initialize : function(){},
    render : function(){
      this.$el.addClass('exton-'+this.model.name+'-cg');
      this.$el.html(this.template(this.model));
      return this;
    }
  });

  views.teamEdit = Backbone.View.extend({
    template : _.template($('#team-edit').html()),
    className : 'form-horizontal',
    tagName : 'form',
    id : 'addTeam',
    events : {
      "click .team-back" : "backClicked",
      "click .team-save" : "saveClicked",
      "click .team-delete" : "deleteClicked",
      "click .exton-add-img" : "addImg",
      "click .exton-del-img" : "delImg"
    },
    initialize : function(){
      this.imgs = this.model.getImages();
      console.log(this.imgs.length);
      this.imgsCount = this.imgs.length ? this.imgs.length : 0;
      console.log(this.model);
    },
    setSpecificImg : function(name,img){
      var img = this.model.get(name);
      if(img !== '' && img !== undefined){
        this.$('.form-actions').before(new views.teamViewImg({ model : {name:name,context:name,controls:false,img:img} }).render().el);
      } else {
        this.$('.form-actions').before(new views.teamViewImg({ model : {name:name,context:name,controls:false,img:img} }).render().el);
      }
    },
    render : function(){
      var self = this;
      this.$el.html(this.template(this.model.toJSON()));
      this.setSpecificImg(TEAM_LOGO,this.model.get(TEAM_LOGO));
      this.setSpecificImg(TEAM_IMG_SEMI,this.model.get(TEAM_IMG_SEMI));
      this.setSpecificImg(TEAM_IMG_FINAL,this.model.get(TEAM_IMG_FINAL));
      this.setSpecificImg(TEAM_IMG_RESULTS,this.model.get(TEAM_IMG_RESULTS));
      this.setSpecificImg(TEAM_IMG_FIRST,this.model.get(TEAM_IMG_FIRST));
      /* carousel */
      if(this.imgs.length!==0){
        this.imgs.forEach(function(img){
          var name = img.substring(img.indexOf("img"),img.indexOf("."));
          console.log(name);
          self.$('.form-actions').before(new views.teamViewImg({ model : {name:name,context:TEAM_IMG_CAROUSEL,controls:true,img:img} }).render().el);
        });
      } else {
        this.addImg();
      }
      return this;
    },
    backClicked : function(e){
      window.location.hash = "#teams";
    },
    saveClicked : function(e){
      this.updateModel();
    },
    deleteClicked : function(){
      this.model.trigger('delete');
      alert('équipe supprimée.');
      window.location.hash = "#teams";
    },
    delImg : function(e){
      var name = $(e.currentTarget).data('cg');
      this.$('.exton-'+name+'-cg').remove();
      this.imgsCount--;
      console.log("[imgsCount]",this.imgsCount);
    },
    addImg : function(){
      this.imgsCount++;
      var n = 'img'+this.imgsCount;
      if(this.imgsCount <= MAX_IMGS){
        this.$('.form-actions').before(new views.teamViewImg({ model : {name:n,context:TEAM_IMG_CAROUSEL,controls:true} }).render().el);
      } else {
        alert('le max pour le carrousel est de '+MAX_IMGS);
      }
    },
    updateModel : function(e){
      var self = this;
      var year = this.$('#year').val();
      var name = this.$('#team-name').val();
      var school = this.$('#team-school').val();
      var web = this.$('#team-website').val();
      var president = this.$('#team-president').val();
      var desc = this.$('#team-desc').val();
      var project = this.$('#project-desc').val();
      var synthesis = this.$('#team-synthesis').val();
      var words = this.$('#president-words').val();
      this.model.set('year',year);
      this.model.set('name',name);
      this.model.set('school',school);
      this.model.set('website',web);
      this.model.set('president',president);
      this.model.set('descAsso',desc);
      this.model.set('descProject',project);
      this.model.set('synthesis',synthesis);
      this.model.set('presWords', words);
      // decrement empty input
      var tempImgsCount = this.imgsCount;
      this.$('.exton-carousel-img').each(function(i,e){ if($(e).val()===''){ tempImgsCount--; }});
      this.model.set('numImgs', tempImgsCount);
    }
  });

  views.pricesLayout = Backbone.View.extend({
    template : _.template($('#prices-layout').html()),
    tagName : "table",
    className : "table table-striped prices",
    initialize : function(){
    },
    render : function(){
      this.$el.html(this.template());
      return this;
    }
  });

  views.priceView = Backbone.View.extend({
    template : _.template($('#price-view').html()),
    tagName : 'tr',
    initialize : function(){},
    render : function(){
      this.$el.html(this.template(this.model.toJSON()));
      return this;
    }
  });

  views.priceEdit = Backbone.View.extend({
    template : _.template($('#price-edit').html()),
    className : 'form-horizontal',
    tagName : 'form',
    id : 'addPrice',
    events : {
      'click .price-back' : 'backClicked',
      'click .price-save' : 'saveClicked',
      'click .price-activated' : 'activatedClicked'
    },
    initialize : function(){
      console.log(this.model.toJSON());
    },
    render : function(){
      this.$el.html(this.template(this.model.toJSON()));
      console.log(typeof this.model.get('activated'));
      if(this.model.get('activated')==="1"){
        this.$('#price-activated').prop('checked',true);
      } else {
        this.$('#price-activated').prop('checked',false);
      }
      var datepickerOptions = {
        weekStart : 1
      };
      this.$('#dpSemiFinal').datepicker(datepickerOptions);
      this.$('#dpFinal').datepicker(datepickerOptions);
      this.$('#dpResults').datepicker(datepickerOptions);
      return this;
    },
    getIsoDate : function(){
      return moment().format('YYYY-MM-DD');
    },
    backClicked : function(){
      window.location.hash = '#prices';
    },
    saveClicked : function(){
      console.log('saved');
      this.updateModel();
    },
    updateModel : function(){
      var year = this.$('#year').val();
      var finalists = this.$('#finalists').val();
      var activated = this.$('#price-activated').prop('checked') ? 1 : 0;
      var link = this.$('#price-vote').val();
      var semiDate = this.$('#semi-date').val();
      var finalDate = this.$('#final-date').val();
      var resultsDate = this.$('#results-date').val();
      var semiDesc = this.$('#semi-desc').val();
      var finalDesc = this.$('#final-desc').val();
      var resultsDesc = this.$('#results-desc').val();
      var phases = {
        init : {date : semiDate, desc:semiDesc},
        final : {date : finalDate, desc:finalDesc},
        results : {date : resultsDate, desc:resultsDesc}
      };
      this.model.set(PRICE_YEAR, year);
      this.model.set(PRICE_FINALISTS, finalists);
      this.model.set(PRICE_VOTE, link);
      this.model.set(PRICE_ACTIVATED, activated);
      this.model.set(PRICE_PHASES, phases);
    },
    activatedClicked : function(e){
      var b = $(e.currentTarget).prop('checked');
      this.model.set(PRICE_ACTIVATED,b ? 1 : 0);
      console.log(b);
    }
  });
  return views;
});