<?php
/********/
/* libs */
/********/
require_once 'autoload.php';
require_once 'NotORM.php';

/*************/
/* Constants */
/*************/
define("USERNAME", "admin");
define("PASSWORD", "p");
define("REALM", "Protected");
define("IMG_PATH", "../img/teams/");

define("TEAM_YEAR", "year");
define("TEAM_LOGO", "logo");
define("TEAM_IMGS", "imgs");
define("TEAM_IMG_SEMI", "imgSemi");
define("TEAM_IMG_FINAL", "imgFinal");
define("TEAM_IMG_RESULTS", "imgResults");
define("TEAM_IMG_FIRST", "imgFirst");
define("TEAM_NAME", "name");
define("TEAM_PRESIDENT", "president");
define("TEAM_SCHOOL", "school");
define("TEAM_WEBSITE", "website");
define("TEAM_DESC_ASSO", "descAsso");
define("TEAM_DESC_PROJECT", "descProject");
define("TEAM_SYNTHESIS", "synthesis");
define("TEAM_PRESIDENT_WORDS", "presWords");

define("PRICE_YEAR", "year");
define("PRICE_ACTIVATED", "activated");
define("PRICE_FINALISTS", "finalists");
define("PRICE_VOTE_LINK", "voteLink");
define("PRICE_PHASES", "phases");
define("PRICE_INIT", "init");
define("PRICE_INIT_DATE", "init_date");
define("PRICE_INIT_DESC", "init_desc");
define("PRICE_FINAL", "final");
define("PRICE_FINAL_DATE", "final_date");
define("PRICE_FINAL_DESC", "final_desc");
define("PRICE_RESULTS", "results");
define("PRICE_RESULTS_DATE", "results_date");
define("PRICE_RESULTS_DESC", "results_desc");
define("PRICE_DATE", "date");
define("PRICE_DESC", "desc");

define("MAX_OLD_EDITIONS", 5);

/******/
/* DB */
/******/
$dsn = "mysql:dbname=exton_facebook;host=localhost";
$username = "exton_facebook";
$password = "aR8JWM2qpUuYJU";

$app = new \Slim\Slim(array(
  'log.enable' => true,
  'debug' => true,
  'log.level' => \Slim\Log::DEBUG,
  'log.writer' => new \Slim\Extras\Log\DateTimeFileWriter(array(
        'path' => './logs',
        'name_format' => 'Y-m-d',
        'message_format' => '%label% - %date% - %message%'
    ))
  ));
$app->getLog()->debug("#app init#");
$pdo = new PDO($dsn, $username, $password);
$app->getLog()->debug("#PDO loaded#");
$db = new NotORM($pdo);
$db->debug = $app->getLog()->debug;
$app->getLog()->debug("#NotORM loaded#");

/*********/
/* utils */
/********/
class Utils {
  private $_app;
  private $_db;

  public function __construct($app,$db){
    $this->_app = $app;
    $this->_db = $db;
  }
  public function priceToArray($price){
    $arr = array(
      "id" => $price["id"],
      PRICE_YEAR => $price[PRICE_YEAR],
      PRICE_ACTIVATED => $price[PRICE_ACTIVATED],
      PRICE_FINALISTS => $price[PRICE_FINALISTS],
      PRICE_VOTE_LINK => $price[PRICE_VOTE_LINK],
      PRICE_PHASES => array(
        PRICE_INIT =>  array( 
          PRICE_DESC => utf8_encode(strip_tags($price[PRICE_INIT_DESC])),
          PRICE_DATE => $price[PRICE_INIT_DATE]
          ),
        PRICE_FINAL =>  array( 
          PRICE_DESC => utf8_encode(strip_tags($price[PRICE_FINAL_DESC])),
          PRICE_DATE => $price[PRICE_FINAL_DATE]
          ),
        PRICE_RESULTS =>  array( 
          PRICE_DESC => utf8_encode(strip_tags($price[PRICE_RESULTS_DESC])),
          PRICE_DATE => $price[PRICE_RESULTS_DATE]
          )
        )
      );
    return $arr;
  }
  public function teamToArray($team){
    $arr = array(
      "id" => $team["id"],
      TEAM_YEAR => $team[TEAM_YEAR],
      TEAM_NAME => utf8_encode($team[TEAM_NAME]),
      TEAM_SCHOOL => utf8_encode($team[TEAM_SCHOOL]),
      TEAM_WEBSITE => $team[TEAM_WEBSITE],
      TEAM_PRESIDENT => utf8_encode($team[TEAM_PRESIDENT]),
      TEAM_PRESIDENT_WORDS => utf8_encode($team[TEAM_PRESIDENT_WORDS]),
      TEAM_LOGO => $team[TEAM_LOGO],
      TEAM_DESC_ASSO => utf8_encode(strip_tags($team[TEAM_DESC_ASSO])),
      TEAM_DESC_PROJECT => utf8_encode(strip_tags($team[TEAM_DESC_PROJECT])),
      TEAM_SYNTHESIS => utf8_encode(strip_tags($team[TEAM_SYNTHESIS])),
      TEAM_IMGS => utf8_encode(strip_tags($team[TEAM_IMGS])),
      TEAM_IMG_SEMI => $team[TEAM_IMG_SEMI],
      TEAM_IMG_FINAL => $team[TEAM_IMG_FINAL],
      TEAM_IMG_RESULTS => $team[TEAM_IMG_RESULTS],
      TEAM_IMG_FIRST => $team[TEAM_IMG_FIRST]
      );
    return $arr;
  }
  public function modelToPrice($price){
    $arr = array(
      PRICE_YEAR => intval($price[PRICE_YEAR]),
      PRICE_ACTIVATED => intval($price[PRICE_ACTIVATED]),
      PRICE_FINALISTS => utf8_decode($price[PRICE_FINALISTS]),
      PRICE_VOTE_LINK => $price[PRICE_VOTE_LINK],
      PRICE_INIT_DESC => utf8_decode($price[PRICE_PHASES][PRICE_INIT][PRICE_DESC]),
      PRICE_INIT_DATE => utf8_decode($price[PRICE_PHASES][PRICE_INIT][PRICE_DATE]),
      PRICE_FINAL_DESC => utf8_decode($price[PRICE_PHASES][PRICE_FINAL][PRICE_DESC]),
      PRICE_FINAL_DATE => utf8_decode($price[PRICE_PHASES][PRICE_FINAL][PRICE_DATE]),
      PRICE_RESULTS_DESC => utf8_decode($price[PRICE_PHASES][PRICE_RESULTS][PRICE_DESC]),
      PRICE_RESULTS_DATE => utf8_decode($price[PRICE_PHASES][PRICE_RESULTS][PRICE_DATE])
    );
    return $arr;
  }
  public function modelToTeam($team){
    $arr = array(
      TEAM_YEAR => intval($team[TEAM_YEAR]),
      TEAM_NAME => utf8_decode($team[TEAM_NAME]),
      TEAM_SCHOOL => utf8_decode($team[TEAM_SCHOOL]),
      TEAM_WEBSITE => $team[TEAM_WEBSITE],
      TEAM_PRESIDENT => utf8_decode($team[TEAM_PRESIDENT]),
      TEAM_PRESIDENT_WORDS => utf8_decode($team[TEAM_PRESIDENT_WORDS]),
      TEAM_DESC_ASSO => utf8_decode($team[TEAM_DESC_ASSO]),
      TEAM_DESC_PROJECT => utf8_decode($team[TEAM_DESC_PROJECT]),
      TEAM_SYNTHESIS => utf8_decode($team[TEAM_SYNTHESIS])
      );
    return $arr;
  }
  public function processImg($name,$teamId){
    $this->_app->getLog()->debug('----->process '.$name);
    $allowedExts = array("jpg", "jpeg", "gif", "png");
    $file = new SplFileInfo($_FILES[$name]["name"]);
    $ext  = pathinfo($file->getFilename(), PATHINFO_EXTENSION);
    $this->_app->getLog()->debug('----->pathinfo '.$ext);
    $this->_app->getLog()->debug($ext);
    if ((($_FILES[$name]["type"] == "image/gif")
      || ($_FILES[$name]["type"] == "image/jpeg")
      || ($_FILES[$name]["type"] == "image/png")
      || ($_FILES[$name]["type"] == "image/pjpeg"))
      && ($_FILES[$name]["size"] < 20000000)
      && in_array($ext, $allowedExts)
      ){
      if ($_FILES[$name]["error"] > 0)
      {
        $this->_app->getLog()->debug("Return Code: " . $_FILES[$name]["error"] . "<br>");
      }
      else
      {
        $this->_app->getLog()->debug("Upload: " . $_FILES[$name]["name"] . "<br>");
        $this->_app->getLog()->debug("Type: " . $_FILES[$name]["type"] . "<br>");
        $this->_app->getLog()->debug("Size: " . ($_FILES[$name]["size"] / 1024) . " kB<br>");
        $this->_app->getLog()->debug("Temp file: " . $_FILES[$name]["tmp_name"] . "<br>");
        $filename = "team" . $teamId . $name . "." . $ext;
        if (file_exists(IMG_PATH . $filename))
        {
          $this->_app->getLog()->debug( $filename . " already exists. ");
        }
        move_uploaded_file($_FILES[$name]["tmp_name"],
          IMG_PATH . $filename);
        $this->_app->getLog()->debug("Stored in: " . IMG_PATH . $filename);
        return $filename;
      }
    }
    else
    {
      $this->_app->getLog()->debug("Invalid file");
    }
    return false;
  }
  public function processImgs ($num,$team){
    $this->_app->getLog()->debug("======> processImgs ");
    $imgs = array();
    if($num===0) { return ''; }
    for($i = 1; $i <= $num; $i++) {
      $temp = $this->processImg("img".$i,$team["id"]);
      if($temp!==false) { $imgs[]= $temp;}
    }
    $this->_app->getLog()->debug("======> end processImgs : ".implode("__", $imgs));
    return implode("__", $imgs);
  }
}
$utils = new Utils($app,$db);

/**************/
/* Middleware */
/**************/
$auth = function() use ($app) {
  $authRequest  = isset($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']);
  $authUser     = $authRequest && $_SERVER['PHP_AUTH_USER'] === USERNAME;
  $authPass     = $authRequest && $_SERVER['PHP_AUTH_PW'] === PASSWORD;
  $res = $app->response();
  if (! $authUser || ! $authPass) {
    $res->status(401);
    $res->header('WWW-Authenticate', sprintf('Basic realm="%s"', REALM));
    $app->halt(403, 'Login failed!');
  }
};

$app->get('/', $auth, function(){
  echo "###random hello";
});

$app->get('/test', $auth, function()use($app){
  echo USERNAME;
});

$app->get('/hello/:name', function ($name) {
  echo "Hello $name";
});

/* misc */
$app->get('/_info', function(){
  $config = '{"version":"0.1.0"}';
  echo $config;
});

/**********/
/* ROUTES */
/**********/
$app->get('/admin', $auth, function() use($app){$app->render('admin.html');});

/* Team CRUD */
$app->get('/teams', function() use($app,$db,$utils){
  $teams = array();
  foreach ($db->teams() as $team) {
    $teams[] = $utils->teamToArray($team);
  }
  echo json_encode($teams);
});
$app->get('/teams/:year', function($year) use ($app,$db,$utils){
  $teams = array();
  foreach ($db->teams()->where('year',$year) as $team) {
    $teams[] = $utils->teamToArray($team);
  }
    echo json_encode($teams);
});
$app->get('/team/:id', function($id)use($db,$utils){
  $team = $utils->teamToArray($db->teams[$id]);
  echo json_encode($team);
});
$app->post('/team', $auth, function()use($app,$utils,$db){
  // init
  $imgs = array();
  $additionalFields = array();

  $allGetVars = $app->request()->headers();
  $arrModel = json_decode(urldecode($allGetVars['QUERY_STRING']),true);
  $app->getLog()->debug(urldecode($allGetVars['QUERY_STRING']));
  // persit and get id
  $res = $db->teams()->insert($utils->modelToTeam($arrModel));
  $app->getLog()->debug('[ team ] insert :'.$res);
  if( $res !== false ){
    $app->getLog()->debug('well inserted');
    $imgsAsText = $utils->processImgs($arrModel['numImgs'],$res);
    if($imgsAsText!==''){ $additionalFields[TEAM_IMGS] = $imgsAsText; }
    $logo = $utils->processImg(TEAM_LOGO,$res["id"]);
    if($logo!==false){ $additionalFields[TEAM_LOGO] = $logo; }
    $semi = $utils->processImg(TEAM_IMG_SEMI,$res["id"]);
    if($semi!=false){ $additionalFields[TEAM_IMG_SEMI] = $semi; }
    $final = $utils->processImg(TEAM_IMG_FINAL,$res["id"]);
    if($final!==false){ $additionalFields[TEAM_IMG_FINAL] = $final; }
    $results = $utils->processImg(TEAM_IMG_RESULTS,$res["id"]);
    if($results!==false){ $additionalFields[TEAM_IMG_RESULTS] = $results; }
    $first = $utils->processImg(TEAM_IMG_FIRST,$res["id"]);
    if($first!==false){ $additionalFields[TEAM_IMG_FIRST] = $first; }
    //persit imgs
    $res->update($additionalFields);
  } else {
    $app->getLog()->debug('not inserted');
  }
  echo json_encode($res);
});
$app->post('/team/:id', $auth, function($id) use ($app,$db,$utils){
  // init
  $imgs = array();
  $additionalFields = array();

  $allGetVars = $app->request()->headers();
  $arrModel = json_decode(urldecode($allGetVars['QUERY_STRING']),true);
  $app->getLog()->debug('[ team ] body :'.urldecode($allGetVars['QUERY_STRING']));
  // persit and get id
  $team = $db->teams[$id];
  $app->getLog()->debug('[ team ] fetched');
  $res = $team->update($utils->modelToTeam($arrModel));
  $app->getLog()->debug(' [ team ] update text '.$res);

  $imgsAsText = $utils->processImgs($arrModel['numImgs'],$team);
  if($imgsAsText!==''){ $additionalFields[TEAM_IMGS] = $imgsAsText; }
  $logo = $utils->processImg(TEAM_LOGO,$team["id"]);
  if($logo!==false){ $additionalFields[TEAM_LOGO] = $logo; }
  $semi = $utils->processImg(TEAM_IMG_SEMI,$team["id"]);
  if($semi!==false){ $additionalFields[TEAM_IMG_SEMI] = $semi; }
  $final = $utils->processImg(TEAM_IMG_FINAL,$team["id"]);
  if($final!==false){ $additionalFields[TEAM_IMG_FINAL] = $final; }
  $results = $utils->processImg(TEAM_IMG_RESULTS,$team["id"]);
  if($results!==false){ $additionalFields[TEAM_IMG_RESULTS] = $results; }
  $first = $utils->processImg(TEAM_IMG_FIRST,$team["id"]);
  if($first!==false){ $additionalFields[TEAM_IMG_FIRST] = $first; }
  $res = $team->update($additionalFields);
  $app->getLog()->debug('[ team ] update imgs '.$res);
  echo json_encode($res);
});
$app->get('/team/delete/:id', $auth, function($id)use($app,$db){
  $app->getLog()->debug('try to delete team '.$id);
  $res = $db->teams[$id]->delete();
  $app->getLog()->debug($res);
  $app->response()->status(204);
});

/* Prices CRUD */
$app->get('/prices', function() use($app,$db,$utils){
  $prices = array();
  foreach ($db->prices() as $price) {
    $prices[]  = $utils->priceToArray($price);
  }
  echo json_encode($prices);
});
$app->get('/currentPrice', function() use($app,$db,$utils){
  $currentPrice = $db->prices()->where('activated', true)->order("year desc")->limit(1);
  // TODO fetch the latest activated by DESC year
  if ($data = $currentPrice->fetch()) {
    echo json_encode($utils->priceToArray($data));
  }
  else{
    echo json_encode(array(
      "status" => false,
      "message" => "Price does not exist"
      ));
  }
});
$app->get('/price/delete/:id', $auth, function($id)use($app,$db){
  $app->getLog()->debug('try to delete price '.$id);
  $res = $db->prices[$id]->delete();
  $app->getLog()->debug($res);
  $app->response()->status(204);
});
$app->get('/price/:id', function($id) use($app,$db,$utils){
  $price = $db->prices()->where('id', $id);
  if ($data = $price->fetch()) {
    echo json_encode($utils->priceToArray($data));
  }
  else{
    echo json_encode(array(
      "status" => false,
      "message" => "Price does not exist"
      ));
  }
});
$app->post('/price', $auth, function()use($app,$db,$utils){
  $allPostVars = $app->request()->getBody();
  parse_str($allPostVars,$output);
  $res = $db->prices()->insert($utils->modelToPrice($output));
  $app->getLog()->debug('[ Price ] add : '.$res);
  echo json_encode($utils->priceToArray($res));
});
$app->post('/price/:id', $auth, function($id)use($app,$utils,$db){
  $allPostVars = $app->request()->getBody();
  parse_str($allPostVars,$output);
  $res = $db->prices[$id]->update($utils->modelToPrice($output));
  $app->getLog()->debug('[ price ] update :'.$res);
  echo json_encode($res);
});

$app->get('/prices/previous', function()use($app,$utils,$db){
  $prices = array();
  $year = date("Y");
  foreach ($db->prices()->where("year < ?", $year)->order('year DESC')->limit(MAX_OLD_EDITIONS) as $price) {
    $prices[]  = $utils->priceToArray($price);
  }
  echo json_encode($prices);
});

$app->run();